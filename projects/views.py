from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_of_projects": projects,
    }
    return render(request, "projects/projectlist.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "specific_project": project,
    }
    return render(request, "projects/projectdetail.html", context)


@login_required
def project_create(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/createproject.html", context)
