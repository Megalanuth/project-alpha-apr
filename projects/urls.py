from django.urls import path
from projects.views import project_list, show_project, project_create


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", project_create, name="create_project"),
]
