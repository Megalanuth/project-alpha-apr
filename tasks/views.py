from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task

# Create your views here.


@login_required
def task_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/createtask.html", context)


@login_required
def my_task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": tasks,
    }
    return render(request, "tasks/mytasklist.html", context)
